﻿

'Call Func_llamarExcel()
Call Objetos_Consulta()
Call Objetos_DatosGenerales()
Call Objetos_Remitente()
Call Objetos_Responsable()
Call Objetos_Adjuntar()
Call Objetos_Conclucion()
Call Objetos_Transporte()
Call Objetos_Medio()
Call Objetos_Deposito()
Call Objetos_Bienes()
Call Objetos_Marco()
Call Objetos_Deudor()
Call Objetos_Lugar()
Call Objetos_Cierre()
Call Objetos_Vehiculo()
Call Objetos_Transportista()
Call Objetos_Intervenido()
Call Objetos_Incautado()


TipoActa = DataTable("TipoActa",2)

Call Func_Consulta(TipoActa, Result)


FechaHora       = DataTable("FechaHora",2)
HoraRegis       = DataTable("HoraRegis",2)
AccionControl   = DataTable("AccionControl",2)
Dependencia     = DataTable("Dependencia",2)
PuestoControl   = DataTable("PuestoControl",2)
PersoSunat      = DataTable("PersoSunat",2)
TipoDeSancion   = DataTable("TipoDeSancion",2)
Correlativo		= DataTable("Correlativo",2)
Anio			= DataTable("Anio",2)
Oouu			= DataTable("Oouu",2)
Serie			= DataTable("Serie",2)


Call Func_DatosGenerales(TipoActa, Correlativo, Anio, Oouu, Serie, FechaHora, HoraRegis, RegistraActa, AccionControl, Dependencia, PuestoControl, PersoSunat, TipoDeSancion)


If TipoActa = "Incidencia" Then

Intervenido     = DataTable("Intervenido",3)
TipoDocInter    = DataTable("TipoDocInter",3)
NumInterve      = DataTable("NumInterve",3)
Direccion       = DataTable("Direccion",3)
Referencia      = DataTable("Referencia",3)

Call Func_Remitente(Intervenido, TipoDocInter, NumInterve, Direccion, Referencia)


FormaIden       = DataTable("FormaIden",4)
TipoDocTra		= DataTable("TipoDocTra",4)
NumTrans        = DataTable("NumTrans",4)

''''''Call Func_Responsable()

Call Func_Transporte(FormaIden, TipoDocTra, NumTrans)

Nplaca          = DataTable("Nplaca",5)
Incauta         = DataTable("Incauta",5)
SerieTran           = DataTable("SerieTran",5)
PtoReferen      = DataTable("PtoReferen",5)
NTarjeta        = DataTable("NTarjeta",5)
PtoDestino      = DataTable("PtoDestino",5)
NPlaSemi        = DataTable("NPlaSemi",5)
Observacion		= DataTable("Observacion",5)	
IncautaRe       = DataTable("IncautaRe",5)
NumTarRe        = DataTable("NumTarRe",5)

Call Func_Medio(Nplaca, Incauta, SerieTran, PtoReferen, NTarjeta, PtoDestino, NPlaSemi, Observacion, IncautaRe, NumTarRe)

TipoAdjunto     = DataTable("TipoAdjunto",6)
RutaDocum       = DataTable("RutaDocum",6)

Call Func_Adjuntar(TipoAdjunto, RutaDocum, TipoActa)


Fecha        = DataTable("Fecha",7)
Hora         = DataTable("Hora",7)
DireccionIn  = DataTable("DireccionIn",7)

Call Func_Conclucion(Fecha, Hora, DireccionIn)


MarcoLegal      = DataTable("MarcoLegal",8)
Ejecucion		= DataTable("Ejecucion",8)

Call Func_Marco(MarcoLegal, Ejecucion)


FamiliaBien = DataTable("FamiliaBien",12)
Variedad    = DataTable("Variedad",12)
Codigo      = DataTable("Codigo",12)
Descripcion = DataTable("Descripcion",12)
TipPresen   = DataTable("TipPresen",12)
CantidadPre = DataTable("CantidadPre",12)
Unidad      = DataTable("Unidad",12)
Peso        = DataTable("Peso",12)
Conserva    = DataTable("Conserva",12)
Concentra   = DataTable("Concentra",12)
NumExtra    = DataTable("NumExtra",12)
ResulPre    = DataTable("ResulPre",12)
ObservaAdi  = DataTable("ObservaAdi",12)

Call Func_Bienes(TipoActa, FamiliaBien, Variedad, Codigo, Descripcion, TipPresen, CantidadPre, Unidad, Peso, Conserva, Concentra, NumExtra, ResulPre, ObservaAdi)

Call Func_Deposito(TipoActa)

'''''''''''''''''''''''''''''''''''''''''''''''PROBATORIA'''''''''''''''''''''

ElseIf TipoActa = "Probatoria" Then


Intervenido = DataTable("Intervenido",9)
TipoDoc     = DataTable("TipoDoc",9)
NumeroIn    = DataTable("NumeroIn",9)


Call Func_Deudor(Intervenido, TipoDoc, NumeroIn)


Call Func_Lugar()

TipoAdjunto     = DataTable("TipoAdjunto",6)
RutaDocum       = DataTable("RutaDocum",6)

Call Func_Adjuntar(TipoAdjunto, RutaDocum, TipoActa)

Call Func_Cierre()


TipoDocVe	= DataTable("TipoDocVe",10)
DocVehic	= DataTable("DocVehic",10)
PlacaVehi	= DataTable("PlacaVehi",10)
Marca		= DataTable("Marca",10)
TipoVehi	= DataTable("TipoVehi",10)

Call Func_Vehiculo(TipoDocVe, DocVehic, PlacaVehi, Marca, TipoVehi)


		
FormaInde	= DataTable("FormaInde",11)
TipoDocTra	= DataTable("TipoDocTra",11)
NumTrans	= DataTable("NumTrans",11)

Call Func_Transportista(FormaInde, TipoDocTra, NumTrans)


FormaInde	= DataTable("FormaInde",11)
TipoDocTra	= DataTable("TipoDocTra",11)
NumTrans	= DataTable("NumTrans",11)


Call Func_Intervenido(FormaInter, TipoDocInter, NumInter)

FamiliaBien = DataTable("FamiliaBien",12)
Variedad    = DataTable("Variedad",12)
Codigo      = DataTable("Codigo",12)
Descripcion = DataTable("Descripcion",12)
TipPresen   = DataTable("TipPresen",12)
CantidadPre = DataTable("CantidadPre",12)
Unidad      = DataTable("Unidad",12)
Peso        = DataTable("Peso",12)
Conserva    = DataTable("Conserva",12)
Concentra   = DataTable("Concentra",12)
NumExtra    = DataTable("NumExtra",12)
ResulPre    = DataTable("ResulPre",12)
ObservaAdi  = DataTable("ObservaAdi",12)

Call Func_Bienes(TipoActa, FamiliaBien, Variedad, Codigo, Descripcion, TipPresen, CantidadPre, Unidad, Peso, Conserva, Concentra, NumExtra, ResulPre, ObservaAdi)

Call Func_Deposito(TipoActa)

Call Func_Incautado()

End If


wait 1
Browser("Otras Actas").Page("Otras Actas").Frame("Deposito").WebButton("Btn_Dep_Final").Click
wait 1
Browser("Otras Actas").Page("Otras Actas").Frame("Deposito").WebButton("Btn_Dep_ConFi").Click

Browser("Sistema Inmovilizacion").CaptureBitmap RutaEvidencias() & "FINAL.png", True
			imagenToWord "SE GUARDO CON EXITO  .-  ", RutaEvidencias() &  "FINAL.png"
wait 1

If TipoActa = "Incidencia" Then
	
Browser("Otras Actas").Page("Otras Actas").Frame("Deposito").WebButton("Btn_Dep_AceFi").Click

Browser("Sistema Inmovilizacion").CaptureBitmap RutaEvidencias() & "FINAL2.png", True
			imagenToWord "FINAL  .-  ", RutaEvidencias() &  "FINAL2.png"
			
Resultado = "OK"
DataTable("Resultado", 13) = Resultado	
	
End If




